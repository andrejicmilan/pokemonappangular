var app = angular.module("PokemonApp",[]);
var pokemonCache = {};

app.controller("GameController", function($scope,$http){
    //wait on load app and render after load
    angular.element(document).ready(function () {
        angular.element('.container').css('display', 'block');
        //show loader for half second(in big app this is needed before load to showe something to user)
        setTimeout($scope.stopLoader,500);
    });
    //Definition of variables
    $scope.name = "";
    $scope.color = "";
    $scope.searchByNameResult;
    $scope.searchByNameImgResult;
    $scope.apiError;
    $scope.searchByColorResult;
    $scope.inputFields = ['color','name'];

    //Definition of functions
    $scope.sort = function (x) {
        $scope.byName = x;
    }

    //function for search by name
    $scope.searchPokemonByName = function () {
        var x = $(event)['0'].target;
        if(x.nodeName === "SPAN"){
            $scope.name = x.innerHTML;
        }else{
            $scope.clearAllResultsExcept('name');
        }
        $scope.name = $scope.makeLowerCase($scope.name);
        //AngularJS ajax call - $http is an AngularJS service for reading data from remote servers.
        if($scope.name){
            $scope.startLoader();
            //If there is a name in the input, we search the cached data,
            //if there are in the cached data we do a if condition, otherwise we send ajax call with else
            if( typeof(pokemonCache[$scope.name]) !== 'undefined' ){
                $scope.searchByNameResult = pokemonCache[$scope.name];
                $scope.searchByNameImgResult = pokemonCache[$scope.name].sprites;
                $scope.stopLoader();
            } else {
                $http({
                    method : "GET",
                    url : "http://pokeapi.co/api/v2/pokemon/" + $scope.name + "/"
                }).then(function mySuccess(response) {
                    $scope.searchByNameResult = response.data;
                    $scope.searchByNameImgResult = response.data.sprites;
                    pokemonCache[$scope.name] = response.data;
                    $scope.stopLoader();
                }, function myError(response) {
                    $scope.apiError = response.statusText;
                    $scope.stopLoader();
                });
            }
        }
    }

    //function for search by color
    $scope.searchPokemonByColor = function () {
        $scope.clearAllResultsExcept('color');
        $scope.color = $scope.makeLowerCase($scope.color);
        if($scope.color){
            $scope.startLoader();
            //AngularJS ajax call - $http is an AngularJS service for reading data from remote servers.
            $http({
                method : "GET",
                url : "http://pokeapi.co/api/v2/pokemon-color/" + $scope.color + "/"
            }).then(function mySuccess(response) {
                $scope.searchByColorResult = response.data;
                $scope.stopLoader();
            }, function myError(response) {
                $scope.apiError = response.statusText;
                $scope.stopLoader();
            });
        }
    }

    //function for clearing fields after some action (search by name or search by color)
    $scope.clearAllResultsExcept = function(skipField){
        for(inputField in $scope.inputFields){
            var field = $scope.inputFields[inputField];
            if(field !== skipField){
                $scope[field] = '';
            }
            angular.element('#'+field+'').val('');
        }
        $scope.searchByColorResult = "";
        $scope.searchByNameImgResult = "";
        $scope.searchByNameResult = "";
        $scope.apiError = "";
    }

    $scope.startLoader = function(){
        angular.element('#loader').show();
    }

    $scope.stopLoader = function(){
        angular.element('#loader').hide();
    }
    
    $scope.makeLowerCase = function(string){
        return angular.lowercase(string);
    };

});

//this can be used in HTML to capitalize first letter of given string
app.filter('capitalize', function() {
    return function(input, scope) {
        if (input!=null){
            input = input.toLowerCase();
        return input.substring(0,1).toUpperCase()+input.substring(1);
        }
    }
});
